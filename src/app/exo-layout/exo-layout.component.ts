import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../service/authentication.service';

@Component({
  selector: 'ns-exo-layout',
  templateUrl: './exo-layout.component.html',
  styleUrls: ['./exo-layout.component.css'],
  moduleId: module.id,
})
export class ExoLayoutComponent implements OnInit {

  constructor(private authServ:AuthenticationService) { }

  ngOnInit() {
  }

  logged() {
    return this.authServ.isLogged();
  }

  logout() {
    this.authServ.logout();
  }
}
