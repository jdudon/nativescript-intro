import { Injectable } from '@angular/core';
import { User } from './entity/user';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private apiUrl = 'http://10.0.2.2:8080/api';

  constructor(private http : HttpClient) { }
  addUser(user:User): Observable<User> {
    return this.http.post<User>(this.apiUrl + '/login_check')
  }
  login(){

  }
}
