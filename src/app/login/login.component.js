"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var authentication_service_1 = require("../service/authentication.service");
var router_1 = require("nativescript-angular/router");
var LoginComponent = /** @class */ (function () {
    function LoginComponent(authService, routerExt) {
        this.authService = authService;
        this.routerExt = routerExt;
        this.user = { email: '', password: '' };
        this.confirm = '';
        this.signup = false;
        this.feedback = '';
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.register = function () {
        var _this = this;
        if (this.user.password === this.confirm) {
            this.authService.addUser(this.user)
                .subscribe(function (user) { return _this.login(); }, function (error) { return _this.feedback = 'Register error'; });
        }
        else {
            this.feedback = 'Password did not match.';
        }
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.authService.login(this.user.email, this.user.password)
            .subscribe(function () { return _this.routerExt.backToPreviousPage(); }, function () { return _this.feedback = 'Login error.'; });
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'ns-login',
            templateUrl: './login.component.html',
            styleUrls: ['./login.component.css'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [authentication_service_1.AuthenticationService,
            router_1.RouterExtensions])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibG9naW4uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBRWxELDRFQUEwRTtBQUMxRSxzREFBK0Q7QUFRL0Q7SUFNRSx3QkFBb0IsV0FBaUMsRUFDM0MsU0FBMEI7UUFEaEIsZ0JBQVcsR0FBWCxXQUFXLENBQXNCO1FBQzNDLGNBQVMsR0FBVCxTQUFTLENBQWlCO1FBTnBDLFNBQUksR0FBUSxFQUFDLEtBQUssRUFBQyxFQUFFLEVBQUUsUUFBUSxFQUFDLEVBQUUsRUFBQyxDQUFDO1FBQ3BDLFlBQU8sR0FBRyxFQUFFLENBQUM7UUFDYixXQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ2YsYUFBUSxHQUFHLEVBQUUsQ0FBQztJQUcwQixDQUFDO0lBRXpDLGlDQUFRLEdBQVI7SUFDQSxDQUFDO0lBRUQsaUNBQVEsR0FBUjtRQUFBLGlCQVlDO1FBWEMsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEtBQUssSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFFdkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztpQkFDaEMsU0FBUyxDQUNSLFVBQUEsSUFBSSxJQUFJLE9BQUEsS0FBSSxDQUFDLEtBQUssRUFBRSxFQUFaLENBQVksRUFDcEIsVUFBQSxLQUFLLElBQUksT0FBQSxLQUFJLENBQUMsUUFBUSxHQUFHLGdCQUFnQixFQUFoQyxDQUFnQyxDQUMxQyxDQUFDO1FBQ04sQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sSUFBSSxDQUFDLFFBQVEsR0FBRyx5QkFBeUIsQ0FBQTtRQUMzQyxDQUFDO0lBRUgsQ0FBQztJQUVELDhCQUFLLEdBQUw7UUFBQSxpQkFNQztRQUxDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO2FBQzFELFNBQVMsQ0FDUixjQUFNLE9BQUEsS0FBSSxDQUFDLFNBQVMsQ0FBQyxrQkFBa0IsRUFBRSxFQUFuQyxDQUFtQyxFQUN6QyxjQUFNLE9BQUEsS0FBSSxDQUFDLFFBQVEsR0FBRyxjQUFjLEVBQTlCLENBQThCLENBQ3JDLENBQUM7SUFDSixDQUFDO0lBaENVLGNBQWM7UUFOMUIsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxVQUFVO1lBQ3BCLFdBQVcsRUFBRSx3QkFBd0I7WUFDckMsU0FBUyxFQUFFLENBQUMsdUJBQXVCLENBQUM7WUFDcEMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1NBQ3BCLENBQUM7eUNBT2dDLDhDQUFxQjtZQUNqQyx5QkFBZ0I7T0FQekIsY0FBYyxDQWtDMUI7SUFBRCxxQkFBQztDQUFBLEFBbENELElBa0NDO0FBbENZLHdDQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFVzZXIgfSBmcm9tICcuLi9lbnRpdHkvdXNlcic7XG5pbXBvcnQgeyBBdXRoZW50aWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlL2F1dGhlbnRpY2F0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gJ25hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlcic7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25zLWxvZ2luJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2xvZ2luLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vbG9naW4uY29tcG9uZW50LmNzcyddLFxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxufSlcbmV4cG9ydCBjbGFzcyBMb2dpbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIHVzZXI6VXNlciA9IHtlbWFpbDonJywgcGFzc3dvcmQ6Jyd9O1xuICBjb25maXJtID0gJyc7XG4gIHNpZ251cCA9IGZhbHNlO1xuICBmZWVkYmFjayA9ICcnO1xuICBcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBhdXRoU2VydmljZTpBdXRoZW50aWNhdGlvblNlcnZpY2UsIFxuICAgIHByaXZhdGUgcm91dGVyRXh0OlJvdXRlckV4dGVuc2lvbnMpIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cbiAgcmVnaXN0ZXIoKSB7ICAgIFxuICAgIGlmKHRoaXMudXNlci5wYXNzd29yZCA9PT0gdGhpcy5jb25maXJtKSB7XG4gICAgICBcbiAgICAgIHRoaXMuYXV0aFNlcnZpY2UuYWRkVXNlcih0aGlzLnVzZXIpXG4gICAgICAgIC5zdWJzY3JpYmUoXG4gICAgICAgICAgdXNlciA9PiB0aGlzLmxvZ2luKCksXG4gICAgICAgICAgZXJyb3IgPT4gdGhpcy5mZWVkYmFjayA9ICdSZWdpc3RlciBlcnJvcidcbiAgICAgICAgKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5mZWVkYmFjayA9ICdQYXNzd29yZCBkaWQgbm90IG1hdGNoLidcbiAgICB9XG4gICAgXG4gIH1cblxuICBsb2dpbigpIHtcbiAgICB0aGlzLmF1dGhTZXJ2aWNlLmxvZ2luKHRoaXMudXNlci5lbWFpbCwgdGhpcy51c2VyLnBhc3N3b3JkKVxuICAgIC5zdWJzY3JpYmUoXG4gICAgICAoKSA9PiB0aGlzLnJvdXRlckV4dC5iYWNrVG9QcmV2aW91c1BhZ2UoKSxcbiAgICAgICgpID0+IHRoaXMuZmVlZGJhY2sgPSAnTG9naW4gZXJyb3IuJ1xuICAgICk7XG4gIH1cblxufVxuIl19