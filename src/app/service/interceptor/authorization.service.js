"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var authentication_service_1 = require("../authentication.service");
var appSetting = require("application-settings");
var AuthorizationInterceptor = /** @class */ (function () {
    function AuthorizationInterceptor(auth) {
        this.auth = auth;
    }
    AuthorizationInterceptor.prototype.intercept = function (req, next) {
        var _this = this;
        var token = appSetting.getString('token');
        if (token) {
            //si on a le token, on clone la requête interceptée.
            var modifiedRequest = req.clone({
                //Dans la requête clonée, on rajoute les headers qu'on veut
                setHeaders: {
                    //en l'occurrence, le Authorization Bearer avec token
                    Authorization: 'Bearer ' + token
                }
            });
            //puis on donne au httpClient la requête modifiée
            return next.handle(modifiedRequest)
                .pipe(
            //Et on lui dit de catcher les erreurs de cette requête
            operators_1.catchError(function (err, caught) {
                //Si l'erreur a un status 401, ça veut dire que notre token
                //est invalide
                if (err.status === 401) {
                    //On dit donc au service authentication de nous logout
                    _this.auth.logout();
                }
                //puis on fait suivre l'erreur
                return rxjs_1.throwError(err);
            }));
        }
        //si ya pas de token, on repasse la requête sans y toucher
        return next.handle(req);
    };
    AuthorizationInterceptor = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [authentication_service_1.AuthenticationService])
    ], AuthorizationInterceptor);
    return AuthorizationInterceptor;
}());
exports.AuthorizationInterceptor = AuthorizationInterceptor;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aG9yaXphdGlvbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXV0aG9yaXphdGlvbi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTJDO0FBRTNDLDZCQUE4QztBQUM5Qyw0Q0FBNEM7QUFDNUMsb0VBQWtFO0FBQ2xFLGlEQUFtRDtBQUtuRDtJQUVFLGtDQUFvQixJQUEwQjtRQUExQixTQUFJLEdBQUosSUFBSSxDQUFzQjtJQUFJLENBQUM7SUFFbkQsNENBQVMsR0FBVCxVQUFVLEdBQXFCLEVBQ3JCLElBQWlCO1FBRDNCLGlCQStCQztRQTdCQyxJQUFJLEtBQUssR0FBRyxVQUFVLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzFDLEVBQUUsQ0FBQSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDVCxvREFBb0Q7WUFDcEQsSUFBSSxlQUFlLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQztnQkFDOUIsMkRBQTJEO2dCQUMzRCxVQUFVLEVBQUU7b0JBQ1YscURBQXFEO29CQUNyRCxhQUFhLEVBQUUsU0FBUyxHQUFDLEtBQUs7aUJBQy9CO2FBQ0YsQ0FBQyxDQUFDO1lBQ0gsaURBQWlEO1lBQ2pELE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLGVBQWUsQ0FBQztpQkFFbEMsSUFBSTtZQUNILHVEQUF1RDtZQUN2RCxzQkFBVSxDQUFDLFVBQUMsR0FBRyxFQUFFLE1BQU07Z0JBQ3JCLDJEQUEyRDtnQkFDM0QsY0FBYztnQkFDZCxFQUFFLENBQUEsQ0FBQyxHQUFHLENBQUMsTUFBTSxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUM7b0JBQ3RCLHNEQUFzRDtvQkFDdEQsS0FBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFDckIsQ0FBQztnQkFDRCw4QkFBOEI7Z0JBQzlCLE1BQU0sQ0FBQyxpQkFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3pCLENBQUMsQ0FBQyxDQUNILENBQUM7UUFDSixDQUFDO1FBQ0QsMERBQTBEO1FBQzFELE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUFuQ1Usd0JBQXdCO1FBSHBDLGlCQUFVLENBQUM7WUFDVixVQUFVLEVBQUUsTUFBTTtTQUNuQixDQUFDO3lDQUd5Qiw4Q0FBcUI7T0FGbkMsd0JBQXdCLENBcUNwQztJQUFELCtCQUFDO0NBQUEsQUFyQ0QsSUFxQ0M7QUFyQ1ksNERBQXdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cEludGVyY2VwdG9yLCBIdHRwUmVxdWVzdCwgSHR0cEhhbmRsZXIgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlLCB0aHJvd0Vycm9yIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBjYXRjaEVycm9yIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgQXV0aGVudGljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vYXV0aGVudGljYXRpb24uc2VydmljZSc7XG5pbXBvcnQgKiBhcyBhcHBTZXR0aW5nIGZyb20gJ2FwcGxpY2F0aW9uLXNldHRpbmdzJztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgQXV0aG9yaXphdGlvbkludGVyY2VwdG9yIGltcGxlbWVudHMgSHR0cEludGVyY2VwdG9ye1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgYXV0aDpBdXRoZW50aWNhdGlvblNlcnZpY2UpIHsgfVxuXG4gIGludGVyY2VwdChyZXE6IEh0dHBSZXF1ZXN0PGFueT4sIFxuICAgICAgICAgICAgbmV4dDogSHR0cEhhbmRsZXIpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIGxldCB0b2tlbiA9IGFwcFNldHRpbmcuZ2V0U3RyaW5nKCd0b2tlbicpO1xuICAgIGlmKHRva2VuKSB7XG4gICAgICAvL3NpIG9uIGEgbGUgdG9rZW4sIG9uIGNsb25lIGxhIHJlcXXDqnRlIGludGVyY2VwdMOpZS5cbiAgICAgIGxldCBtb2RpZmllZFJlcXVlc3QgPSByZXEuY2xvbmUoe1xuICAgICAgICAvL0RhbnMgbGEgcmVxdcOqdGUgY2xvbsOpZSwgb24gcmFqb3V0ZSBsZXMgaGVhZGVycyBxdSdvbiB2ZXV0XG4gICAgICAgIHNldEhlYWRlcnM6IHtcbiAgICAgICAgICAvL2VuIGwnb2NjdXJyZW5jZSwgbGUgQXV0aG9yaXphdGlvbiBCZWFyZXIgYXZlYyB0b2tlblxuICAgICAgICAgIEF1dGhvcml6YXRpb246ICdCZWFyZXIgJyt0b2tlblxuICAgICAgICB9XG4gICAgICB9KTtcbiAgICAgIC8vcHVpcyBvbiBkb25uZSBhdSBodHRwQ2xpZW50IGxhIHJlcXXDqnRlIG1vZGlmacOpZVxuICAgICAgcmV0dXJuIG5leHQuaGFuZGxlKG1vZGlmaWVkUmVxdWVzdClcbiAgICAgIC8vT24gcmFqb3V0ZSB1biBwaXBlIHN1ciBsJ09ic2VydmFibGUgcXVpIHN1cnZlaWxsZSBsYSByZXF1w6p0ZVxuICAgICAgLnBpcGUoXG4gICAgICAgIC8vRXQgb24gbHVpIGRpdCBkZSBjYXRjaGVyIGxlcyBlcnJldXJzIGRlIGNldHRlIHJlcXXDqnRlXG4gICAgICAgIGNhdGNoRXJyb3IoKGVyciwgY2F1Z2h0KSA9PiB7XG4gICAgICAgICAgLy9TaSBsJ2VycmV1ciBhIHVuIHN0YXR1cyA0MDEsIMOnYSB2ZXV0IGRpcmUgcXVlIG5vdHJlIHRva2VuXG4gICAgICAgICAgLy9lc3QgaW52YWxpZGVcbiAgICAgICAgICBpZihlcnIuc3RhdHVzID09PSA0MDEpIHtcbiAgICAgICAgICAgIC8vT24gZGl0IGRvbmMgYXUgc2VydmljZSBhdXRoZW50aWNhdGlvbiBkZSBub3VzIGxvZ291dFxuICAgICAgICAgICAgdGhpcy5hdXRoLmxvZ291dCgpO1xuICAgICAgICAgIH1cbiAgICAgICAgICAvL3B1aXMgb24gZmFpdCBzdWl2cmUgbCdlcnJldXJcbiAgICAgICAgICByZXR1cm4gdGhyb3dFcnJvcihlcnIpO1xuICAgICAgICB9KVxuICAgICAgKTtcbiAgICB9XG4gICAgLy9zaSB5YSBwYXMgZGUgdG9rZW4sIG9uIHJlcGFzc2UgbGEgcmVxdcOqdGUgc2FucyB5IHRvdWNoZXJcbiAgICByZXR1cm4gbmV4dC5oYW5kbGUocmVxKTtcbiAgfVxuICBcbn1cbiJdfQ==