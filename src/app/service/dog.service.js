"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var environment_1 = require("../../environments/environment");
var DogService = /** @class */ (function () {
    function DogService(http) {
        this.http = http;
        this.apiUrl = environment_1.environment.serverUrl + '/api/dog/';
    }
    DogService.prototype.findAll = function () {
        return this.http.get(this.apiUrl);
    };
    DogService.prototype.findByUser = function () {
        return this.http.get(this.apiUrl + 'user');
    };
    DogService.prototype.add = function (dog) {
        return this.http.post(this.apiUrl, dog);
    };
    DogService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], DogService);
    return DogService;
}());
exports.DogService = DogService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG9nLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJkb2cuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEyQztBQUMzQyw2Q0FBa0Q7QUFHbEQsOERBQTJEO0FBSzNEO0lBR0Usb0JBQW9CLElBQWU7UUFBZixTQUFJLEdBQUosSUFBSSxDQUFXO1FBRjNCLFdBQU0sR0FBRyx5QkFBVyxDQUFDLFNBQVMsR0FBQyxXQUFXLENBQUM7SUFFWixDQUFDO0lBRXhDLDRCQUFPLEdBQVA7UUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQVEsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFHRCwrQkFBVSxHQUFWO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFRLElBQUksQ0FBQyxNQUFNLEdBQUMsTUFBTSxDQUFDLENBQUM7SUFFbEQsQ0FBQztJQUVELHdCQUFHLEdBQUgsVUFBSSxHQUFPO1FBQ1QsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFNLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDL0MsQ0FBQztJQWpCVSxVQUFVO1FBSHRCLGlCQUFVLENBQUM7WUFDVixVQUFVLEVBQUUsTUFBTTtTQUNuQixDQUFDO3lDQUl5QixpQkFBVTtPQUh4QixVQUFVLENBa0J0QjtJQUFELGlCQUFDO0NBQUEsQUFsQkQsSUFrQkM7QUFsQlksZ0NBQVUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgRG9nIH0gZnJvbSAnLi4vZW50aXR5L2RvZyc7XG5pbXBvcnQge2Vudmlyb25tZW50fSBmcm9tICcuLi8uLi9lbnZpcm9ubWVudHMvZW52aXJvbm1lbnQnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBEb2dTZXJ2aWNlIHtcbiAgcHJpdmF0ZSBhcGlVcmwgPSBlbnZpcm9ubWVudC5zZXJ2ZXJVcmwrJy9hcGkvZG9nLyc7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwOkh0dHBDbGllbnQpIHsgfVxuXG4gIGZpbmRBbGwoKTogT2JzZXJ2YWJsZTxEb2dbXT4ge1xuICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0PERvZ1tdPih0aGlzLmFwaVVybCk7XG4gIH1cblxuICBcbiAgZmluZEJ5VXNlcigpOiBPYnNlcnZhYmxlPERvZ1tdPiB7XG4gICAgcmV0dXJuIHRoaXMuaHR0cC5nZXQ8RG9nW10+KHRoaXMuYXBpVXJsKyd1c2VyJyk7XG5cbiAgfVxuXG4gIGFkZChkb2c6RG9nKTogT2JzZXJ2YWJsZTxEb2c+IHtcbiAgICByZXR1cm4gdGhpcy5odHRwLnBvc3Q8RG9nPih0aGlzLmFwaVVybCwgZG9nKTtcbiAgfVxufVxuIl19