"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var operators_1 = require("rxjs/operators");
var environment_1 = require("../../environments/environment");
/**
 * Le localStorage n'existe pas dans les applications mobiles, à la
 * place, on peut utiliser le application-settings qui permet de
 * stocker des paramètres pour une application spécifique.
 */
var appSetting = require("application-settings");
var AuthenticationService = /** @class */ (function () {
    function AuthenticationService(http) {
        this.http = http;
        this.apiUrl = environment_1.environment.serverUrl + '/api';
    }
    AuthenticationService.prototype.addUser = function (user) {
        return this.http.post(this.apiUrl + '/user/', user);
    };
    AuthenticationService.prototype.login = function (email, password) {
        return this.http.post(this.apiUrl + '/login_check', {
            username: email,
            password: password
        }).pipe(operators_1.tap(function (data) {
            if (data.token) {
                appSetting.setString('token', data.token);
            }
        }));
    };
    AuthenticationService.prototype.logout = function () {
        appSetting.remove('token');
    };
    AuthenticationService.prototype.isLogged = function () {
        return appSetting.hasKey('token');
    };
    AuthenticationService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], AuthenticationService);
    return AuthenticationService;
}());
exports.AuthenticationService = AuthenticationService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aGVudGljYXRpb24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImF1dGhlbnRpY2F0aW9uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkM7QUFDM0MsNkNBQWtEO0FBR2xELDRDQUFxQztBQUNyQyw4REFBMkQ7QUFFM0Q7Ozs7R0FJRztBQUNILGlEQUFtRDtBQU9uRDtJQUdFLCtCQUFvQixJQUFlO1FBQWYsU0FBSSxHQUFKLElBQUksQ0FBVztRQUYzQixXQUFNLEdBQUcseUJBQVcsQ0FBQyxTQUFTLEdBQUMsTUFBTSxDQUFDO0lBRVAsQ0FBQztJQUV4Qyx1Q0FBTyxHQUFQLFVBQVEsSUFBUztRQUNmLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBTyxJQUFJLENBQUMsTUFBTSxHQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUMzRCxDQUFDO0lBRUQscUNBQUssR0FBTCxVQUFNLEtBQVksRUFBRSxRQUFlO1FBQ2pDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBTSxJQUFJLENBQUMsTUFBTSxHQUFHLGNBQWMsRUFBRTtZQUN2RCxRQUFRLEVBQUUsS0FBSztZQUNmLFFBQVEsRUFBRSxRQUFRO1NBQ25CLENBQUMsQ0FBQyxJQUFJLENBQ0wsZUFBRyxDQUFDLFVBQUEsSUFBSTtZQUNOLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUNkLFVBQVUsQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUM1QyxDQUFDO1FBQ0gsQ0FBQyxDQUFDLENBQ0gsQ0FBQztJQUNKLENBQUM7SUFFRCxzQ0FBTSxHQUFOO1FBQ0UsVUFBVSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUM3QixDQUFDO0lBRUQsd0NBQVEsR0FBUjtRQUNFLE1BQU0sQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3BDLENBQUM7SUE1QlUscUJBQXFCO1FBSGpDLGlCQUFVLENBQUM7WUFDVixVQUFVLEVBQUUsTUFBTTtTQUNuQixDQUFDO3lDQUl5QixpQkFBVTtPQUh4QixxQkFBcUIsQ0E2QmpDO0lBQUQsNEJBQUM7Q0FBQSxBQTdCRCxJQTZCQztBQTdCWSxzREFBcUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgVXNlciB9IGZyb20gJy4uL2VudGl0eS91c2VyJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IHRhcCB9IGZyb20gXCJyeGpzL29wZXJhdG9yc1wiO1xuaW1wb3J0IHtlbnZpcm9ubWVudH0gZnJvbSAnLi4vLi4vZW52aXJvbm1lbnRzL2Vudmlyb25tZW50JztcblxuLyoqXG4gKiBMZSBsb2NhbFN0b3JhZ2UgbidleGlzdGUgcGFzIGRhbnMgbGVzIGFwcGxpY2F0aW9ucyBtb2JpbGVzLCDDoCBsYVxuICogcGxhY2UsIG9uIHBldXQgdXRpbGlzZXIgbGUgYXBwbGljYXRpb24tc2V0dGluZ3MgcXVpIHBlcm1ldCBkZVxuICogc3RvY2tlciBkZXMgcGFyYW3DqHRyZXMgcG91ciB1bmUgYXBwbGljYXRpb24gc3DDqWNpZmlxdWUuXG4gKi9cbmltcG9ydCAqIGFzIGFwcFNldHRpbmcgZnJvbSAnYXBwbGljYXRpb24tc2V0dGluZ3MnO1xuXG5cblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgQXV0aGVudGljYXRpb25TZXJ2aWNlIHtcbiAgcHJpdmF0ZSBhcGlVcmwgPSBlbnZpcm9ubWVudC5zZXJ2ZXJVcmwrJy9hcGknO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cDpIdHRwQ2xpZW50KSB7IH1cblxuICBhZGRVc2VyKHVzZXI6VXNlcik6IE9ic2VydmFibGU8VXNlcj4ge1xuICAgIHJldHVybiB0aGlzLmh0dHAucG9zdDxVc2VyPih0aGlzLmFwaVVybCsgJy91c2VyLycsIHVzZXIpO1xuICB9XG5cbiAgbG9naW4oZW1haWw6c3RyaW5nLCBwYXNzd29yZDpzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHJldHVybiB0aGlzLmh0dHAucG9zdDxhbnk+KHRoaXMuYXBpVXJsICsgJy9sb2dpbl9jaGVjaycsIHtcbiAgICAgIHVzZXJuYW1lOiBlbWFpbCxcbiAgICAgIHBhc3N3b3JkOiBwYXNzd29yZFxuICAgIH0pLnBpcGUoXG4gICAgICB0YXAoZGF0YSA9PiB7XG4gICAgICAgIGlmKGRhdGEudG9rZW4pIHtcbiAgICAgICAgICBhcHBTZXR0aW5nLnNldFN0cmluZygndG9rZW4nLCBkYXRhLnRva2VuKTtcbiAgICAgICAgfVxuICAgICAgfSlcbiAgICApO1xuICB9XG4gIFxuICBsb2dvdXQoKSB7XG4gICAgYXBwU2V0dGluZy5yZW1vdmUoJ3Rva2VuJyk7XG4gIH1cblxuICBpc0xvZ2dlZCgpIHtcbiAgICByZXR1cm4gYXBwU2V0dGluZy5oYXNLZXkoJ3Rva2VuJyk7XG4gIH1cbn1cbiJdfQ==