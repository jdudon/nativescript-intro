import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Dog } from '../entity/dog';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DogService {
  private apiUrl = environment.serverUrl+'/api/dog/';

  constructor(private http:HttpClient) { }

  findAll(): Observable<Dog[]> {
    return this.http.get<Dog[]>(this.apiUrl);
  }

  
  findByUser(): Observable<Dog[]> {
    return this.http.get<Dog[]>(this.apiUrl+'user');

  }

  add(dog:Dog): Observable<Dog> {
    return this.http.post<Dog>(this.apiUrl, dog);
  }
}
