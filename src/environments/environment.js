"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * L'ip en 10.0.2.2 pointe sur le localhost de la machine depuis
 * un émulateur. L'émulateur n'a pas accès directement à localhost
 */
exports.environment = {
    production: false,
    serverUrl: 'http://10.0.2.2:8080'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW52aXJvbm1lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJlbnZpcm9ubWVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBOzs7R0FHRztBQUNVLFFBQUEsV0FBVyxHQUFHO0lBQ3pCLFVBQVUsRUFBRSxLQUFLO0lBQ2pCLFNBQVMsRUFBRSxzQkFBc0I7Q0FDbEMsQ0FBQztBQUVGOzs7Ozs7R0FNRztBQUNILG1FQUFtRSIsInNvdXJjZXNDb250ZW50IjpbIlxuLyoqXG4gKiBMJ2lwIGVuIDEwLjAuMi4yIHBvaW50ZSBzdXIgbGUgbG9jYWxob3N0IGRlIGxhIG1hY2hpbmUgZGVwdWlzXG4gKiB1biDDqW11bGF0ZXVyLiBMJ8OpbXVsYXRldXIgbidhIHBhcyBhY2PDqHMgZGlyZWN0ZW1lbnQgw6AgbG9jYWxob3N0XG4gKi9cbmV4cG9ydCBjb25zdCBlbnZpcm9ubWVudCA9IHtcbiAgcHJvZHVjdGlvbjogZmFsc2UsXG4gIHNlcnZlclVybDogJ2h0dHA6Ly8xMC4wLjIuMjo4MDgwJ1xufTtcblxuLypcbiAqIEZvciBlYXNpZXIgZGVidWdnaW5nIGluIGRldmVsb3BtZW50IG1vZGUsIHlvdSBjYW4gaW1wb3J0IHRoZSBmb2xsb3dpbmcgZmlsZVxuICogdG8gaWdub3JlIHpvbmUgcmVsYXRlZCBlcnJvciBzdGFjayBmcmFtZXMgc3VjaCBhcyBgem9uZS5ydW5gLCBgem9uZURlbGVnYXRlLmludm9rZVRhc2tgLlxuICpcbiAqIFRoaXMgaW1wb3J0IHNob3VsZCBiZSBjb21tZW50ZWQgb3V0IGluIHByb2R1Y3Rpb24gbW9kZSBiZWNhdXNlIGl0IHdpbGwgaGF2ZSBhIG5lZ2F0aXZlIGltcGFjdFxuICogb24gcGVyZm9ybWFuY2UgaWYgYW4gZXJyb3IgaXMgdGhyb3duLlxuICovXG4vLyBpbXBvcnQgJ3pvbmUuanMvZGlzdC96b25lLWVycm9yJzsgIC8vIEluY2x1ZGVkIHdpdGggQW5ndWxhciBDTEkuXG4iXX0=